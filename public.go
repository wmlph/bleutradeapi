package bleutrade

import (
	"encoding/json"
	"errors"
	"github.com/franela/goreq"
	"log"
	"time"
)

type (
	GetCurrenciesT []struct {
		Currency        string
		CurrencyLong    string
		MinConfirmation json.Number
		TxFee           json.Number
		IsActive        bool `json:",string"`
		CoinType        string
	}

	GetMarketsT []struct {
		MarketCurrency     string
		BaseCurrency       string
		MarketCurrencyLong string
		BaseCurrencyLong   string
		MinTradeSize       json.Number
		MarketName         string
		IsActive           bool `json:",string"`
	}

	GetTickerT []TickerT
	TickerT    struct {
		Bid  json.Number
		Ask  json.Number
		Last json.Number
	}
	GetMarketSummariesT []GetMarketSummaryT
	GetMarketSummaryT   struct {
		MarketName string
		PrevDay    json.Number
		High       json.Number
		Low        json.Number
		Last       json.Number
		Average    json.Number
		Volume     json.Number
		BaseVolume json.Number
		TimeStamp  string
		Bid        json.Number
		Ask        json.Number
		IsActive   bool `json:",string"`
	}
	GetMarketSummariesMap map[string]GetMarketSummaryT

	OrderBookT struct {
		Buy  []OrderT
		Sell []OrderT
	}
	OrderT struct {
		Quantity json.Number
		Rate     json.Number
	}
)

func handleErr(response jsonResponse) error {
	if response.Success != true {
		return errors.New(response.Message)
	}
	return nil
}

func (c *Bleutrade) Get(method string, output interface{}) (err error) {
	mutex.Lock()
	defer mutex.Unlock()
	var response jsonResponse
	params := make(map[string]string)
	err = c.publicRequest(method, params, &response)
	if err != nil {
		return
	}
	if err = handleErr(response); err != nil {
		return
	}
	err = json.Unmarshal(response.Result, output)
	if err != nil {
		return
	}
	return
}

func (c *Bleutrade) GetCurrencies() (currencies GetCurrenciesT, err error) {
	err = c.Get("getcurrencies", &currencies)
	return
}

func (c *Bleutrade) GetMarkets() (Markets GetMarketsT, err error) {
	err = c.Get("getmarkets", &Markets)
	if err != nil {
		return
	}
	gm := GetMarketsT{}
	for k := range Markets {
		v := Markets[k]
		if v.IsActive {
			gm = append(gm, v)
		}
	}
	Markets = gm
	return
}

func (c *Bleutrade) GetTicker(code string) (Ticker TickerT, err error) {
	gt := GetTickerT{}
	err = c.Get("getticker&market="+code, &gt)
	if err != nil {
		return
	}
	Ticker = gt[0]
	return
}

func (c *Bleutrade) GetMarketSummaries() (MarketSummaries GetMarketSummariesMap, err error) {
	ms := GetMarketSummariesT{}
	err = c.Get("getmarketsummaries", &ms)
	gm := GetMarketSummariesMap{}
	for k := range ms {
		v := ms[k]
		a, _ := v.Ask.Float64()
		b, _ := v.Bid.Float64()
		if a == 0.0 && b == 0.0 {
			continue
		}
		if !v.IsActive {
			continue
		}
		gm[v.MarketName] = v
	}
	MarketSummaries = gm
	return
}

func (c *Bleutrade) GetMarketSummary(code string) (MarketSummary GetMarketSummaryT, err error) {
	gm := GetMarketSummariesT{}
	err = c.Get("getmarketsummary?market="+code, &gm)
	if err != nil {
		return
	}
	MarketSummary = gm[0]
	return
}

func (c *Bleutrade) GetOrderBook(code string) (OrderBook OrderBookT, err error) {
	err = c.Get("getorderbook?market="+code+"&type=ALL&depth=1", &OrderBook)
	return
}

func (c Bleutrade) publicRequest(method string, params map[string]string, output interface{}) error {
	url := c.Config.PublicURL + method
	res, err := goreq.Request{Uri: url, Accept: "application/json", Compression: goreq.Gzip(), Timeout: 10 * time.Second}.Do()
	if err != nil {
		return err
	}

	s, err := res.Body.ToString()
	if err != nil {
		log.Println(err)
	}

	err = json.Unmarshal([]byte(s), output)
	if err != nil {
		return err
	}

	return nil
}
