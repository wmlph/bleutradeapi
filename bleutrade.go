package bleutrade

import (
	"encoding/json"
	"log"
	"sync"
)

// only data types in this file see public.go for Public API and private.go for the Private API

type (
	Bleutrade struct {
		Config Config
	}

	jsonResponse struct {
		Success bool `json:",string"`
		Message string
		Result  json.RawMessage
	}
)

var (
	nonce int64
	mutex = sync.Mutex{}
)

func New(configFile string) Bleutrade {
	c := Bleutrade{}
	var err error
	c.Config = LoadConfig(configFile)

	if err != nil {
		log.Println(err)
	}
	return c
}
