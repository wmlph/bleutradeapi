package bleutrade

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type (
	ExchangeAPI interface {
		Summary() (SummariesMap, error)
		Sell(string, float64, float64) (string, error)
		Buy(string, float64, float64) (string, error)
		Transfer(string, float64, ExchangeAPI) error
		GetAddress(string) (string, error)
		BuyPrice(string) (float64, float64, error)
		SellPrice(string) (float64, float64, error)
		GetBalance(string) (float64, error)
		IsActive(string) bool
		ActiveOrders() []GlobalOrder
	}
	GlobalOrder struct {
		Quantity  float64
		Price     float64
		ID        string
		Direction string
		Code      string
	}

	SummariesMap struct {
		Name    string
		Results map[string]Summary
	}
	Summary struct {
		MarketID      string
		Code          string
		SecondaryCode string
		Bid           json.Number
		Ask           json.Number
		BidQuantity   json.Number
		AskQuantity   json.Number
		ViewUrl       string
		Volume        json.Number
	}

	Config struct {
		Key        string
		Secret     string
		PublicURL  string
		PrivateURL string
		ViewURL    string
		MinBuy     float64
		ShitCoins  map[string]bool
	}
)

func LoadConfig(filename string) Config {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalln(err)
	}
	var c Config
	err = json.Unmarshal(b, &c)
	if err != nil {
		log.Fatalln(err)
	}
	return c
}
