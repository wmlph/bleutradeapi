package bleutrade

import (
	"github.com/davecgh/go-spew/spew"
	"testing"
)

var bleutrade *Bleutrade

var orderid string

func TestSummary(t *testing.T) {
	c, err := bleutrade.Summary()
	if err != nil {
		t.Error(err)
	}
	spew.Dump(c)
}

func init() {
	c := New("config.json")
	bleutrade = &c
}
