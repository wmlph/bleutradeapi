# Bleutrade API wrapper README #

To use the wrapper you'll need to create a json config file and pass that name to the constructor:


```
#!go

bleu:=bleutrade.New("config.json")
```

The config file should look like this:


```
#!json

{
    "Key": "1234567890abcdef1234567890abcdef",
    "Secret": "abcdef1234567890abcdef1234567890",
    "PrivateURL": "https://bleutrade.com/api/v2/",
    "PublicURL": "https://bleutrade.com/api/v2/public/",
    "ViewURL": "https://bleutrade.com/exchange/%s"
}
```

The ViewURL is optional.