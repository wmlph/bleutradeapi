package bleutrade

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"log"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/franela/goreq"
)

type (
	BuySellT struct {
		OrderId string
	}

	GetOpenOrdersT []struct {
		OrderId            string
		Exchange           string
		Type               string
		Quantity           json.Number
		QuantityRemaining  json.Number
		QuantityBaseTraded json.Number
		Price              json.Number
		Status             string
		Created            string
		Comments           string
	}

	GetBalancesT []BalanceT
	BalanceT     struct {
		Currency      string
		Balance       json.Number
		Available     json.Number
		Pending       json.Number
		CryptoAddress string
		IsActive      bool `json:",string"`
	}
	OrderIDT struct {
		OrderID string `json:"orderid"`
	}
)

func (c *Bleutrade) PrivateBuy(code string, rate float64, quantity float64) (OrderId OrderIDT, err error) {
	if rate*quantity < 0.0001 {
		return
	}
	r := strconv.FormatFloat(rate, 'f', 8, 64)
	q := strconv.FormatFloat(quantity, 'f', 8, 64)
	// oid := BuySellT{}
	err = c.privateParseData("market/buylimit?market="+code+"&quantity="+q+"&rate="+r, &OrderId)
	if err != nil {
		log.Println(err)
	}
	return
}

func (c *Bleutrade) PrivateSell(code string, rate float64, quantity float64) (OrderId OrderIDT, err error) {
	if rate*quantity < 0.0001 {
		return
	}
	r := strconv.FormatFloat(rate, 'f', 8, 64)
	q := strconv.FormatFloat(quantity, 'f', 8, 64)

	err = c.privateParseData("market/selllimit?market="+code+"&quantity="+q+"&rate="+r, &OrderId)
	if err != nil {
		log.Println(err)
	}
	return
}

func (c *Bleutrade) Cancel(orderid string) (err error) {
	var response jsonResponse
	err = c.request("market/cancel?orderid="+orderid, &response)
	if err != nil {
		return
	}
	if err = handleErr(response); err != nil {
		return
	}
	return
}

func (c *Bleutrade) GetOpenOrders() (OpenOrders GetOpenOrdersT, err error) {
	err = c.privateParseData("market/getopenorders", &OpenOrders)
	if err != nil {
		log.Println(err)
		return
	}
	return
}

func (c *Bleutrade) GetBalances() (Balances GetBalancesT, err error) {
	err = c.privateParseData("account/getbalances", &Balances)
	return
}

func (c *Bleutrade) PrivateGetBalance(code string) (Balance BalanceT, err error) {
	err = c.privateParseData("account/getbalance?currency="+code, &Balance)
	return
}

func (c *Bleutrade) Withdraw(code string, quantity float64, address string) (err error) {
	s := ""
	q := strconv.FormatFloat(quantity, 'f', 8, 64)
	err = c.privateParseData("account/withdraw?currency="+code+"&quantity="+q+"&address="+address, &s)
	return
}

// generate hmac-sha512 hash, hex encoded
func (c Bleutrade) sign(payload string) string {
	mac := hmac.New(sha512.New, []byte(c.Config.Secret))
	mac.Write([]byte(payload))
	return hex.EncodeToString(mac.Sum(nil))
}

// returns a url encoded string to be signed on send
func (c Bleutrade) encodePostData(method string, params map[string]string) string {
	result := ""

	// params are unordered, but after method and nonce
	if len(params) > 0 {
		v := url.Values{}
		for key := range params {
			v.Add(key, params[key])
		}
		result = result + "&" + v.Encode()
	}

	return result
}

// make a call to the jsonrpc api, marshal into v
func (c Bleutrade) request(method string, v interface{}) error {
	mutex.Lock()
	defer mutex.Unlock()

	sep := "?"
	if strings.Contains(method, "?") {
		sep = "&"
	}
	method = method + sep + "apikey=" + c.Config.Key

	uri := c.Config.PrivateURL + method
	req := goreq.Request{Method: "POST", Uri: uri, ContentType: "application/x-www-form-urlencoded", Accept: "Application/json", Timeout: 10 * time.Second}

	req.AddHeader("apisign", c.sign(uri))

	res, err := req.Do()
	if err != nil {
		log.Println(err)
		return err
	}
	s, err := res.Body.ToString()
	if err != nil {
		log.Println(err)
		return err
	}
	err = json.Unmarshal([]byte(s), v)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func init() {
	nonce = time.Now().Unix()
}

func (a Bleutrade) privateParseData(method string, o interface{}) (err error) {
	var response jsonResponse
	err = a.request(method, &response)
	if err != nil {
		return
	}
	if err = handleErr(response); err != nil {
		return
	}
	err = json.Unmarshal(response.Result, o)
	return
}
